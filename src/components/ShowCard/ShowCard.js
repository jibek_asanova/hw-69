import React, {useEffect, useReducer} from 'react';
import axios from "axios";
import './ShowCard.css';

const FETCH_SHOW_INFORMATION_REQUEST = 'FETCH_SHOW_INFORMATION_REQUEST';
const FETCH_SHOW_INFORMATION_SUCCESS = 'FETCH_SHOW_INFORMATION_SUCCESS';
const FETCH_SHOW_INFORMATION_FAILURE = 'FETCH_SHOW_INFORMATION_FAILURE';

const fetchShowInformationRequest = () => ({type: FETCH_SHOW_INFORMATION_REQUEST});
const fetchShowInformationSuccess = shows => ({type: FETCH_SHOW_INFORMATION_SUCCESS, payload: shows});
const fetchShowInformationFailure = error => ({type: FETCH_SHOW_INFORMATION_FAILURE, payload: error});

const initialState = {
    loading: false,
    showContent: null,
    error: null
};

const reducer = (state, action) => {
    switch (action.type) {
        case FETCH_SHOW_INFORMATION_REQUEST:
            return {...state, loading: true}
        case FETCH_SHOW_INFORMATION_SUCCESS:
            return {...state, loading: false, showContent: action.payload}
        case FETCH_SHOW_INFORMATION_FAILURE:
            return {...state, loading: false, error: action.payload}
        default:
            return state;
    }
}

const ShowCard = ({match}) => {
    const id = match.params.id;
    const [state, dispatch] = useReducer(reducer, initialState);

    useEffect(() => {
        const fetchData = async () => {
            dispatch(fetchShowInformationRequest());
            try {
                const response = await axios.get('https://api.tvmaze.com/shows/' + id);
                const showInfo = response.data;
                dispatch(fetchShowInformationSuccess(showInfo));

            } catch (e) {
                dispatch(fetchShowInformationFailure());
            }
        };

        fetchData().catch(console.error);
    }, [id]);


    return state.showContent && (
        <div className="Content">
            <img src={state.showContent.image.medium} alt={state.showContent.name}/>
            <div className="Text">
                <h2>{state.showContent.name}</h2>
                <p>{state.showContent.summary.replace(/<\/?[^>]+(>|$)/g, "")}</p>
            </div>
        </div>
    );
};

export default ShowCard;