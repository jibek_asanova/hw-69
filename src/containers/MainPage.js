import React, {useReducer} from 'react';
import {Autocomplete} from "@material-ui/lab";
import {TextField} from "@material-ui/core";
import axios from "axios";
import {useHistory} from "react-router-dom";
import Spinner from "../components/UI/Spinner/Spinner";

const FETCH_SHOW_REQUEST = 'FETCH_SHOW_REQUEST';
const FETCH_SHOW_SUCCESS = 'FETCH_SHOW_SUCCESS';
const FETCH_SHOW_FAILURE = 'FETCH_SHOW_FAILURE';

const fetchShowRequest = () => ({type: FETCH_SHOW_REQUEST});
const fetchShowSuccess = shows => ({type: FETCH_SHOW_SUCCESS, payload: shows});
const fetchShowFailure = error => ({type: FETCH_SHOW_FAILURE, payload: error});

const initialState = {
    loading: false,
    shows: [],
    error: null
};

const reducer = (state, action) => {
    switch (action.type) {
        case FETCH_SHOW_REQUEST:
            return {...state, loading: true}
        case FETCH_SHOW_SUCCESS:
            return {...state, loading: false, shows: action.payload}
        case FETCH_SHOW_FAILURE:
            return {...state, loading: false, error: action.payload}
        default:
            return state;
    }
}

const MainPage = () => {
    const history = useHistory();
    const [state, dispatch] = useReducer(reducer, initialState);


    const fetchShow = async (val) => {
        if(val.length >= 3) {
            dispatch(fetchShowRequest());
            try {
                const response = await axios.get('http://api.tvmaze.com/search/shows?q=' + val);
                const showsData = response.data;
                const array = [];
                showsData.forEach(show => (
                    array.push(show.show)
                ));
                dispatch(fetchShowSuccess(array));
            } catch (e) {
                dispatch(fetchShowFailure());
            }
        }

    };

    const getIdOfElement = (val) => {
        if (val !== null) {
            history.replace('/shows/' + val.id);
        }

    };

    return (
        <>
            <h2>TV Shows</h2>
            <Autocomplete
                id="combo-box-demo"
                options={state.shows}
                getOptionLabel={(option) => option.name}
                style={{width: 300}}
                renderInput={(params) => <TextField {...params} label="Combo box" variant="outlined"
                                                    onChange={e => fetchShow(e.target.value)}/>}
                onChange={(event, value) => getIdOfElement(value)}
            />
            {state.loading ? <Spinner/> : null}
        </>
    );
};

export default MainPage;