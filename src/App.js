import MainPage from "./containers/MainPage";
import {Route, Switch} from "react-router-dom";
import ShowCard from "./components/ShowCard/ShowCard";

const App = () => (
    <div className="App">
        <div className="Autocomplete">
            <MainPage/>
        </div>
        <div className="ShowCard">
            <Switch>
                <Route path="/" exact render={() => <h1>Select show!</h1>}/>
                <Route path="/shows/:id" component={ShowCard}/>
            </Switch>
        </div>
    </div>


);

export default App;
